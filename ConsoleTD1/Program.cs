﻿using System;
using System.Collections.Generic;
using System.Text;
using LibraryTD1;
using LibraryTD1.Entreprise;
using PersonneLibrary;

namespace ConsoleTD1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Afficher les accents et autres caractères
            Console.OutputEncoding = Encoding.UTF8;
            Service s1 = new Service(1, "Comptat");

            Employe p1 = new Employe(1, "DUPONT", "Charles", new DateTime(1998, 04, 12), 1980, s1, 5, Personne.Typegenre.Homme);
            Employe p2 = new Employe(2, "KERBAN", "Henriette", new DateTime(1981, 09, 24), 1500, s1, 20, Personne.Typegenre.Femme);
            Employe p3 = new Employe(3, "CHAMPOT", "Paul", new DateTime(1981, 09, 17), 1500, s1, Personne.Typegenre.Homme);
            Employe p4 = new Employe(4, "JOULIE", "Alexandra", new DateTime(1987, 11, 21), 2100, s1, Personne.Typegenre.Femme);

            Entreprise tardos = new Entreprise();

            tardos.AjouterPersonne(p1);
            tardos.AjouterPersonne(p2);
            tardos.AjouterPersonne(p3);
            tardos.AjouterPersonne(p4);


            foreach (KeyValuePair<string, double> keyValuePair in tardos.CalculDeMoyenneMinMax())
            {
                Console.WriteLine(keyValuePair);
            }
        }
    }
}