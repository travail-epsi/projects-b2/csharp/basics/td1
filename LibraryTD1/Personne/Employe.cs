﻿using System;
using LibraryTD1;

namespace PersonneLibrary
{
    public class Employe : Personne
    {
        private static double tauxImposition = 0.2;

        #region Attributs

        private int nbRTT;

        #endregion

        #region Propriétés

        public int NbRTT
        {
            get => nbRTT;
            set
            {
                if (value < 0 || value > 26) throw new Exception("Wrong RTT number");

                nbRTT = value;
            }
        }

        public override string Identite => $"{base.Identite}, RTT: {nbRTT}";

        public override double TauxImposition => tauxImposition;

        #endregion

        #region Constructeur

        public Employe(int id, string nom, string prenom, DateTime dateDeNaissance, double salaireBrut, Service service,
            Typegenre genre)
            : base(id, nom, prenom, dateDeNaissance, salaireBrut, service, genre)
        {
        }

        public Employe(int id, string nom, string prenom, DateTime dateDeNaissance, double salaireBrut, Service service,
            int nbRTT, Typegenre genre)
            : base(id, nom, prenom, dateDeNaissance, salaireBrut, service, genre)
        {
            NbRTT = nbRTT;
        }

        #endregion
    }
}