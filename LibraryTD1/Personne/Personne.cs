﻿using System;

namespace LibraryTD1
{
    public abstract class Personne
    {
        public enum Typegenre
        {
            Homme,
            Femme
        }
        
        #region Attributs

        private int id;
        private string nom;
        private string prenom;
        private DateTime dateDeNaissance;
        private int age;
        private double salaireBrut;
        private double salaireNet;
        private Service service;
        private Typegenre genre;


        #endregion

        #region Propriétés "classiques"

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nom
        {
            get { return nom; }
            private set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            private set { prenom = value; }
        }

        public DateTime DateDeNaissance
        {
            get { return dateDeNaissance; }
            private set { dateDeNaissance = value; }
        }

        public double SalaireBrut
        {
            get { return salaireBrut; }
            set { salaireBrut = value; }
        }

        public int Age
        {
            get
            {
                // Le calcul n'est pas très précis mais ça fonctionne ...
                return (int) Math.Floor(DateTime.Now.Subtract(DateDeNaissance).TotalDays / 365);
            }
        }

        public Service Service
        {
            get { return service; }
            set
            {
                if (value != null) service = value;
            }
        }

        public double SalaireNet => SalaireBrut * 0.8;

        public Typegenre Genre
        {
            get => genre;
            set => genre = value;
        }

        #endregion

        #region Propriétés automatiques

        public string Adresse { get; set; }
        public abstract double TauxImposition { get; }

        public virtual double SupplementSalaire
        {
            get { return 0; }
        }

        #endregion

        #region Constructeurs

        public Personne(int id, string nom, string prenom, DateTime dateDeNaissance, Service service, Typegenre genre)
        {
            Id = id;
            Nom = nom;
            Prenom = prenom;
            DateDeNaissance = dateDeNaissance;
            Service = service;
            Genre = genre;
        }

        public Personne(int id, string nom, string prenom, DateTime dateDeNaissance, double salaireBrut,
            Service service, Typegenre genre) :
            this(id, nom, prenom, dateDeNaissance, service, genre)
        {
            SalaireBrut = salaireBrut;
            age = Age;
            salaireNet = SalaireNet;
        }

        #endregion

        #region Méthodes

        public virtual string Identite =>
            $"{id}, {nom}, {prenom}, {Age} ans, {salaireBrut:C} brut, {SalaireNet:C} net, {service.afficher}";

        #endregion
    }
}