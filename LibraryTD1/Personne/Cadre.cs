﻿using System;
using LibraryTD1;

namespace PersonneLibrary
{
    public class Cadre : Personne
    {
        public enum TypeStatus
        {
            Technique = 1,
            Administratif,
            Juridique,
            Financier
        }


        private static double tauxImposition = 0.25;

        #region Attributs

        private double prime;
        private TypeStatus typeStatus;

        #endregion

        #region Propriétés

        public double Prime
        {
            get { return prime; }
            set
            {
                if (value >= 0 && value <= 6000)
                {
                    prime = value;
                }
                else
                {
                    throw new Exception("");
                }
            }
        }

        public TypeStatus Statut
        {
            get { return typeStatus; }
        }

        public override double TauxImposition
        {
            get { return tauxImposition; }
        }


        public override double SupplementSalaire
        {
            get { return Prime; }
        }


        public override string Identite
        {
            get { return $"{base.Identite}, Prime: {prime:C}, Statut={typeStatus}"; }
        }

        #endregion

        #region Constructeur

        public Cadre(int id, string nom, string prenom, DateTime dateDeNaissance, double salaireBrut, Service service,
            double prime, TypeStatus typeStatus, Typegenre genre)
            : base(id, nom, prenom, dateDeNaissance, salaireBrut, service, genre)
        {
            Service = service;
            Prime = prime;
            this.typeStatus = typeStatus;
        }

        #endregion
    }
}