﻿using System;

namespace LibraryTD1.Exception
{
    public class PersonneInvalidAttributeException : PersonneException
    {
        public PersonneInvalidAttributeException()
        {
        }

        public PersonneInvalidAttributeException(string attribut) : base($" L'attribut : {attribut} est faux")
        {
        }
        
        
    }
}