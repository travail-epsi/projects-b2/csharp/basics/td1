﻿using System;

namespace LibraryTD1.Exception
{
    public class PersonneNullReferenceException : PersonneException
    {
        public PersonneNullReferenceException(string message) : base(message)
        {
        }

        public PersonneNullReferenceException()
        {
        }
    }
}