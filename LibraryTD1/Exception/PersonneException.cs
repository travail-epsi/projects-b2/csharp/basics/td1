﻿using System;

namespace LibraryTD1.Exception
{
    public class PersonneException : System.Exception
    {
        public PersonneException(string message) : base(message)
        {
        }

        public PersonneException() : base("Something went wrong with Personne")
        {
        }
    }
}