﻿using System;

namespace LibraryTD1
{
    public class Service
    {
        #region Attributs

        private int id;
        private String libelle;

        #endregion


        #region Propriétés

        public int Id
        {
            get { return id; }
            private set { id = value; }
        }

        public String Libelle
        {
            get { return libelle; }
            private set { libelle = value; }
        }

        #endregion


        #region Constructeurs

        public Service(int id, String libelle)
        {
            Id = id;
            Libelle = libelle;
        }

        #endregion

        public String afficher => $"{Id}, {Libelle}";
    }
}