﻿using System;
using System.Collections.Generic;
using LibraryTD1.Exception;
using static LibraryTD1.Personne;

namespace LibraryTD1.Entreprise
{
    public class Entreprise
    {
        #region Attributs

        #endregion

        #region Propriétés Automatiques

        public List<Personne> Personnes { get; }

        public List<Service> Services { get; }

        #endregion

        public Entreprise()
        {
            Personnes = new List<Personne>();
            Services = new List<Service>();
        }

        #region Gestion Personnel

        public bool AjouterPersonne(Personne personne)
        {
            Personnes.Add(personne);

            return true;
        }

        public bool SupprimerPersonne(Personne personne)
        {
            if (Personnes.Contains(personne))
            {
                return Personnes.Remove(personne);
            }

            throw new PersonneNullReferenceException("Cette personne n'existe pas");
        }

        public bool SupprimerPersonne(int id)
        {
            if (Personnes.Exists(personne => personne.Id == id))
            {
                Personne personneTrouvee = RechercherPersonne(id);

                return Personnes.Remove(personneTrouvee);
            }

            throw new PersonneNullReferenceException("Cet id n'a pas été trouvé");
        }

        public Personne RechercherPersonne(int id)
        {
            if (Personnes.Exists(personne => personne.Id == id))
            {
                return Personnes.Find(personne => personne.Id == id);
            }

            throw new PersonneNullReferenceException("Cet id n'a pas été trouvé");
        }

        public List<Personne> RechercherPersonneCommencantPar(String nom)
        {
            if (Personnes.Exists(personne => personne.Nom.StartsWith(nom)))
            {
                return Personnes.FindAll(personne => personne.Nom.StartsWith(nom));
            }

            throw new PersonneInvalidAttributeException(nom);
        }

        public List<Personne> RechercherPersonneParService(String nomService)
        {
            if (Personnes.Exists(personne => personne.Service.Libelle == nomService))
            {
                return Personnes.FindAll(personne => personne.Service.Libelle == nomService);
            }

            throw new PersonneInvalidAttributeException(nomService);
        }


        public void AfficherPersonnes()
        {
            Personnes.ForEach(delegate(Personne personne) { Console.WriteLine(personne.Identite); });
        }

        public double CalculMasseSalariale()
        {
            double masseSalariale = 0;
            foreach (Personne personne in Personnes)
            {
                masseSalariale += personne.SalaireBrut;
            }

            return masseSalariale;
        }

        public Dictionary<string, double> CalculMasseSalarialeParService()
        {
            Dictionary<string, double> masseSalarialeParServices = new Dictionary<string, double>();

            foreach (Service service in Services)
            {
                double masseSalariale = 0;

                List<Personne> personnes = RechercherPersonneParService(service.Libelle);

                foreach (Personne personne in personnes)
                {
                    masseSalariale += personne.SalaireBrut;
                }

                masseSalarialeParServices.Add(service.Libelle, masseSalariale);
            }

            return masseSalarialeParServices;
        }

        public Dictionary<int, double> CalculMasseSalarialeParServiceV2()
        {
            Dictionary<int, double> masseSalarialeParServices = new Dictionary<int, double>();

            foreach (Personne personne in Personnes)
            {
                if (masseSalarialeParServices.ContainsKey(personne.Service.Id))
                {
                    masseSalarialeParServices[personne.Service.Id] += personne.SalaireBrut;
                }
                else
                {
                    masseSalarialeParServices.Add(personne.Service.Id, personne.SalaireBrut);
                }
            }

            return masseSalarialeParServices;
        }

        public Dictionary<string, double> CalculDeMoyenneMinMax()
        {
            Dictionary<string, double> minMaxMoy = new Dictionary<string, double>();

            double min = 0, max = 0, sum = 0;
            foreach (Personne personne in Personnes)
            {
                if (max < personne.SalaireBrut)
                {
                    max = personne.SalaireBrut;
                }

                if (Personnes.IndexOf(personne) == 0)
                {
                    min = personne.SalaireBrut;
                }
                else if (min > personne.SalaireBrut)
                {
                    min = personne.SalaireBrut;
                }

                sum += personne.SalaireBrut;
            }


            minMaxMoy.Add("Moyenne", sum / Personnes.Count);
            minMaxMoy.Add("Maximum", max);
            minMaxMoy.Add("Minimum", min);
            minMaxMoy.Add("Ecart", max - min);
            return minMaxMoy;
        }

        public Dictionary<string, double> SalaireParGenre()
        {
            Dictionary<string, double> salaireParGenre = new Dictionary<string, double>();
            int nbHomme = 0, nbFemme = 0;
            double sumHomme = 0, sumFemme = 0;
            foreach (Personne personne in Personnes)
            {
                switch (personne.Genre)
                {
                    case Typegenre.Homme:
                        sumHomme += personne.SalaireBrut;
                        nbHomme++;
                        break;
                    case Typegenre.Femme:
                        sumFemme += personne.SalaireBrut;
                        nbFemme++;
                        break;
                    default:
                        throw new PersonneException($"Mauvais genre pour cette personne {personne.Identite}");
                }
            }

            salaireParGenre.Add("Moyenne Homme", sumHomme / nbHomme);
            salaireParGenre.Add("Moyenne Femme", sumFemme / nbFemme);
            salaireParGenre.Add("Difference", sumHomme - sumFemme);

            return salaireParGenre;
        }

        #endregion

        #region Gestion Services

        public bool AjouterService(Service service)
        {
            Services.Add(service);

            return true;
        }

        public bool SupprimerService(Service service)
        {
            if (Services.Contains(service))
            {
                return Services.Remove(service);
            }

            throw new PersonneNullReferenceException("Cette personne n'existe pas");
        }

        public bool SupprimerService(int id)
        {
            Service serviceTrouve = RechercherService(id);

            return Services.Remove(serviceTrouve);
        }

        public Service RechercherService(int id)
        {
            if (Services.Exists(service => service.Id == id))
            {
                return Services.Find(service => service.Id == id);
            }

            throw new PersonneNullReferenceException("Cet id n'a pas été trouvé");
        }

        public void AfficherServices()
        {
            Services.ForEach(delegate(Service service) { Console.WriteLine(service.afficher); });
        }

        #endregion
    }
}